from django.urls import path, include
from tastypie.api import Api

from django_voipstack_hotel_api.django_voipstack_hotel_api.api.resources import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.api.resources import *

v1_api = Api(api_name="v1")

# API GENERAL
v1_api.register(ApiAuthCustomerApiKeyResource())
v1_api.register(ApiAuthCustomerResource())

# API VOIP HOTEL
v1_api.register(CallsResource())
v1_api.register(CallsByRangeResource())
v1_api.register(CrmResource())

urlpatterns = [
    path("", include(v1_api.urls)),
]
